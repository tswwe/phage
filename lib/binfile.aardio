﻿//binfile 二进制文件解析
/**
字节 1BYTE = 8bit
字   1WORD = 2BYTE
**/
namespace binfile{
	
}

binfile = class {
	ctor(path){
		import io;
		import string;
		this.file = io.open(path, "rb");
		this.pos = 0;                  //指针在第几字节。read系函数将从下一字节（含下一字节）开始读取。
		this.size = this.file.size(1); //文件大小（字节数)
		..table.gc(this,"close");      //析构函数
	};
	read = function(arg) begin            //00 0A 02 00 00    ==>  {0x0; 0xA; 0x2; 0x0; 0x0}
		var uniBytes = arg.uniBytes or 2; //单元字节数
		var n = arg.n or 1;               //读取几个单元
		
		var dataType = {
			[1] = "BYTE";
			[2] = "WORD";
			[4] = "INT";
			[8] = "LONG64";
		};
		var queryStr = string.format("{%s num[%s]={1}}", dataType[uniBytes], n);
		this.file.seek("set", this.pos);
		var data = this.file.read(eval(queryStr)).num;  //返回数组。但若到达尾部，返回null
		this.pos += uniBytes*n;
		return data; 
	end;
	readStr = function(arg) begin         //11 12 13 14 15    ==>  "ABCDE"
		var isUnicode = arg.isUnicode or false; //字符编码，默认ASCII，true为UNICODE
		var n = arg.n or 1;				        //读取几个字符
		
		var uniBytes = isUnicode ? 2 or 1;      //单个ASCII字符占1字节，UNICODE字符为2字节
		var tb = this.read(uniBytes=uniBytes; n=n);
		if(tb==null){return null};              //若到达尾部，返回null
		var retstr = "";
		if(!isUnicode){
			retstr = string.pack(tb);           //数值转ASCII的方法
		}else {
			for(i=1;#tb;1){
				var v = tb[i];
				//if(v==0xD){v=0xA};//\r => \n 回车变换行
				retstr = string.concat(retstr, string.format("\u%.4X", v));
			}
			retstr = eval("'" ++ retstr ++ "'");//数值转UNICODE的方法
		}
		return retstr; 
	end;
	readWord = function(n=1) begin              //同read，但当n=1时返回数值，n>1时返回数组
		var data = this.read(uniBytes=2; n=n);
		if(data==null){return null};            //若到达尾部，返回null
		return (n>1 ? data : data[1]); 
	end;
	readStrA = function(n=1) begin              //同readStr，但针对ASCII
		return this.readStr(isUnicode=false; n=n);
	end;
	readStrW = function(n=1) begin              //同readStr，但针对UNICODE
		return this.readStr(isUnicode=true; n=n);
	end;
	skip0 = function() begin //跳过为0的字节
		var n = 0;
		while(true){
			if(this.eof()) return n; 
			var v = this.read(uniBytes=1)[1];
			if(v==0 or v==null){
				n++;
				continue;
			}else {
				this.pos -= 1;
				return n;
			};
		};
	end;
	readNextStrA = function() begin //从非0字节开始，读到下一个0字节之间的文本
		var n = 0;
		while(true){
			var v = this.readWord();
			if(v){
				n++;
			}else {
				break;
			};
		};
		this.pos -= (n+1)*2;
		return this.readStrA(n);
	end;
	readNextStrW = function() begin //从非0字节开始，读到下一个0字节之间的文本
		var n = 0;
		while(true){
			var v = this.readWord();
			if(v){
				n++;
			}else {
				break;
			};
		};
		this.pos -= (n+1)*2;
		return this.readStrW(n);
	end;
	dump = function(arg) begin //DUMP文件的二进制数据
		var uniBytes = arg.uniBytes or 1;
		var n = arg.n or 400;
		var noSkip0 = arg.noSkip0 or false;
		
		var str = '0xADDR ADDRE 0xVALU VALUE STRA STRW';
		for(i=1;n;1){
			var val = this.read(uniBytes=uniBytes);
			if(val==null){break};
			val = val[1];
			
			if(val==0 and !noSkip0){continue};
			var addr = this.pos - uniBytes;
			var strA = string.pack(val);
			var strW = eval("'\u"++string.format("%.4X", val)++"'");
			
			str = string.concat(str, '\r\n',
				                string.format(uniBytes==2 ? '0x%.4X  %.5i  0x%.4X  %.5i  %s   %s' : '0x%.4X  %.5i  0x%.2X    %.3i    %s   %s',addr, addr, val, val, "", strW)
				 );
		}
		return str; 
	end;
	eof = function() begin  //判断是否到达文件末
		return this.pos>=this.size; 
	end;
	close = function() begin  //释放文件，无需手工调用
		this.file.close();	
	end;
}